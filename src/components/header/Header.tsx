import React, { useEffect, useState } from 'react';
import s from './header.module.scss'

const Header = () => {
  const [ clock, setСlock ] = useState('')

  const refreshClock = () => {
    const currDate = new Date().getDate().toString();
    const currMount = new Date().toLocaleString('EN', { month: 'long' })
    const currTime = new Date().toLocaleTimeString().slice(0, -3);
    setСlock(`${currDate}${' '}${currMount}${' '}${currTime}`)
  }

  useEffect(() => {
    const timerId = setInterval(refreshClock, 0);
    return function cleanup() {
      clearInterval(timerId);
    };
  }, [])

  return (
    <header className={ s.header_top }>
      <div className={ s.wrapper_icon_booking }>
        <div className={ s.icon_booking } />
      </div>
      <button className={ s.btn_top }>Office</button>
      <button className={ s.btn_top }>Room</button>
      <div className={ s.wrapper_clock }>
        <time>
          {clock}
        </time>
      </div>
      <button className={ s.wrapper_calendar }>
        <div className={ s.calendar } />
      </button>
    </header>
  )
}

export { Header }
