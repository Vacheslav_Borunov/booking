import React from 'react';
import './App.css';
import { BodyContent } from './components/body_content/BodyContent';
import { Header } from './components/header/Header';

function App() {
  return (
    <div>
      <Header />
      <BodyContent />
    </div>
  );
}

export default App;
