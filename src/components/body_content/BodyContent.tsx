import React from 'react';
import { RegistrationBooking } from '../registration_booking/RegistrationBooking';
import { Timeline } from '../timeline/Timeline';

const styles = {
  wrapper: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '95vh',
  },

}
const BodyContent = () => (
  <section style={ styles.wrapper }>
    <Timeline />
    <RegistrationBooking />
  </section>
)

export { BodyContent }
