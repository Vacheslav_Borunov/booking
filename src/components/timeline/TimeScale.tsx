import React from 'react';
import s from './timeline.module.scss'

const TimeScale = () => (
  <div className={ s.time_scale }>
    <span className={ s.time_scale_clock }>00:00</span>
    <div className={ s.time_scale_unit }>
      <div className={ s.time_scale_unit_half } />
    </div>
    <div className={ s.time_scale_unit }>
      <div className={ s.time_scale_unit_half } />
    </div>
  </div>
)

export { TimeScale }
