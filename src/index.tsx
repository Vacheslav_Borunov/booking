import React from 'react';
import ReactDOM, { createRoot } from 'react-dom/client';
import './index.css';
import { createBrowserHistory } from 'history';
import { unstable_HistoryRouter as HistoryRouter } from 'react-router-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';

const container = document.getElementById('root');
const root = createRoot(container);
const history = createBrowserHistory({ window });

root.render(
  <React.StrictMode>
    <HistoryRouter history={ history }>
      <App />
    </HistoryRouter>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
