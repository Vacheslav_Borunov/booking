import React from 'react';
import s from './registr_booking.module.scss'

const RegistrationBooking = () => (
  <div className={ s.wrapper_reg_booking }>
    <div className={ s.wrapper_time_window }>
      <div>
        <span>Free till:</span>
        <p>23:59</p>
        <span>Remain:</span>
        <p>06:59</p>
      </div>
    </div>
    <button className={ s.btn_add }>Add Booking</button>
    <div className={ s.wrapper_block_btn_instant }>
      <span>Instant booking</span>
      <div className={ s.wrapper_btn_instant }>
        <button className={ s.btn_instant }>+ 10 min</button>
        <button className={ s.btn_instant }>+ 30 min</button>
      </div>
    </div>
  </div>
)

export { RegistrationBooking }
